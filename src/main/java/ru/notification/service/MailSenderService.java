package ru.notification.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.notification.dto.MailInfoDto;

@Service
@Slf4j
@RequiredArgsConstructor
public class MailSenderService {

    private final JavaMailSender javaMailSender;

    public void sendJavaMail(MailInfoDto mailInfo) {

        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(mailInfo.getMailFrom());
        mailMessage.setTo(mailInfo.getMailTo());
        mailMessage.setSubject(mailInfo.getSubject());
        mailMessage.setText(mailInfo.getMessage());

        javaMailSender.send(mailMessage);
        log.info("MailSenderService: message were sent from {} to {}", mailInfo.getMailFrom(), mailInfo.getMailTo());
    }
}