package ru.notification.service.listener;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import ru.notification.dto.MailInfoDto;
import ru.notification.service.MailSenderService;

@Component
@RequiredArgsConstructor
public class NotificationListener {

    private final MailSenderService mailSenderService;

    @RabbitListener(queues = "notificationQueue")
    public void listen(@RequestBody MailInfoDto mailInfo) {
        mailSenderService.sendJavaMail(mailInfo);
    }
}