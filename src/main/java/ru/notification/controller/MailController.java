package ru.notification.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.notification.dto.MailInfoDto;
import ru.notification.service.MailSenderService;

import javax.validation.Valid;

@RestController
@RequestMapping("/mail")
@RequiredArgsConstructor
public class MailController {

    private final MailSenderService mailSenderService;

    @PostMapping("/send")
    public ResponseEntity<Void> sendMail(@Valid @RequestBody MailInfoDto mailInfo) {
        mailSenderService.sendJavaMail(mailInfo);
        return ResponseEntity.ok().build();
    }
}