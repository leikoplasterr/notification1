FROM openjdk:17-jdk-slim
ADD /target/notification1-1.0-SNAPSHOT.jar notification1.jar
ENTRYPOINT ["java","-jar","notification1.jar"]